flatpak-spawn --host \
	podman run \
	--security-opt label=disable \
	--userns=keep-id \
	-p 8888:8888 \
	-v .:/home/jovyan/work \
	-e JUPYTER_ENABLE_LAB=yes \
	jupyter/scipy-notebook
