# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import numpy as np
from scipy import interpolate
import xmltodict

def tableIn(_pathIn):
    '''
    Parameters
    ----------
    _pathIn : string
        File location of a TunerStudio msqpart file

    Returns
    -------
    _dfIn : pandas dataframe
        table from msqpart file

    '''
    with open(_pathIn) as _fIn:
        _xmlIn =  xmltodict.parse(_fIn.read())
        _rpmIn =  np.array([z[0] for z in [x.strip().split() for x in _xmlIn['msq']['page'][5]['constant'][2]['#text'].split('\n')]])
        _loadIn = np.array([z[0] for z in [x.strip().split() for x in _xmlIn['msq']['page'][5]['constant'][1]['#text'].split('\n')]])
        _dfIn = pd.DataFrame(
            data=np.array([[float(y) for y in x.strip().split()] for x in _xmlIn['msq']['page'][5]['constant'][0]['#text'].split('\n')]),
            columns = _loadIn,
            index = _rpmIn
            )

    return _dfIn

def interpTable(_tableIn, _rpmIn, _loadIn):
    '''
    Parameters
    ----------
    _tableIn : pandas dataframe
        table from msqpart file
    _rpmIn : float
        location on the RPM axis to interpolate from the table
    _loadIn : float
        location on the load axis to interpolate from the table

    Returns
    -------
    _interpTable : float
        value interpolated from _tableIn at given location

    '''
    _interpTable = interpolate.interp2d(
        x = _tableIn.columns.to_numpy().astype('float'),
        y = _tableIn.index.to_numpy().astype('float'),
        z = _tableIn.to_numpy().astype('float')
        )
    return _interpTable(_rpmIn, _loadIn)[0]

#%%

_pathIn = 'C:\\Users\\Jeff\\Documents\\TunerStudioProjects\\coffeeCar\\'
ve1 = tableIn(_pathIn + 'veTable1Tbl_2021-04-23_19.37.11.msqpart')
ve2 = tableIn(_pathIn + 'veTable3Tbl_2021-04-23_19.37.25.msqpart')

for _x in ve2.columns.to_numpy():
    for _y in ve2.index.to_numpy():
       ve2[_x][_y] = interpTable(ve1,float(_x),float(_y))